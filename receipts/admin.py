from django.contrib import admin
from receipts.models import ExpenseCategory, Receipt, Account


@admin.register(ExpenseCategory)
class ReceiptsAdmin(admin.ModelAdmin):
    list_display = ("name", "owner", "id")


@admin.register(Receipt)
class ReceiptsAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    )


@admin.register(Account)
class ReceiptsAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "number", "owner")
